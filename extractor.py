# Kidle-highlight extractor 0.1
# it will take your clippings.txt, extract just highlights into a new file

# STEPS (1 method)
# 1. Open the file in Python
# 2. Find those lines which contains anything else than highlighted line
#   a. starts with '='
#   b. and line after it
#   c. starts with '-'
# 3. Leave empty line, work as space btw highlights

from fpdf import FPDF

def extractor(name):
    with open(name, 'r') as file:
        lines = file.readlines()

    with open('highlights_final.txt', 'w') as file:
        count = 0
        for line in lines:
            count += 1
            if count%5 == 0:
                file.write(f"{line}\n")

# def convToPdf(name):
#     pdf = FPDF()
#     pdf.add_page()
#     pdf.set_font("Arial", size = 15)

#     file = open(name, 'r')
#     for line in file:
#         pdf.cell(200, 10, txt = line, ln = 1, align = 'C')
    
#     pdf.output('highlights.pdf')



fileName = "highlights.txt" #input("Enter your file name: ")
extractor(fileName)



    # with open(name, 'r') as file:
    #     lines = file.readlines()

    # with open('highlights_final.txt', 'w') as file:
    #     count = 0
    #     for line in lines:
    #         if not (line.startswith('=') or line.startswith('-') or line.startswith(first)):
    #             if not line == "\n":
    #                 count += 1
    #                 file.write(str(count) + ". " + line + "\n")